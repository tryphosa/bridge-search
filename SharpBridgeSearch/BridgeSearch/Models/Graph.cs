using System;
using System.Collections.Generic;
using System.Linq;

namespace BridgeSearch.Models
{
    public class Graph
    {
        public Vertex[] Vertices { get; }

        public Graph(IEnumerable<Vertex> vertices)
        {
            Vertices = vertices.ToArray();
        }

        public void DepthFirstSearch(
            Action<Vertex, Vertex> onForwardPass = null,
            Action<Vertex, Vertex> onBackwardPass = null,
            Action<Vertex, Vertex> onBackwardEdge = null)
        {
            var uncoloredVertices = Vertices.Where(v => v.Color == null).ToArray();
            while (uncoloredVertices.Any())
            {
                var randomVertex = uncoloredVertices[Program.RNG.Next(uncoloredVertices.Length)];

                DepthFirstSearch(randomVertex, onForwardPass, onBackwardPass, onBackwardEdge);
                uncoloredVertices = Vertices.Where(v => v.Color == null).ToArray();
            }
        }

        private static void DepthFirstSearch(
            Vertex v,
            Action<Vertex, Vertex> onForwardPass = null,
            Action<Vertex, Vertex> onBackwardPass = null,
            Action<Vertex, Vertex> onBackwardEdge = null)
        {
            var stack = new Stack<Vertex>();
            v.Color = true;
            stack.Push(v);
            while (stack.Any())
            {
                var vertex = stack.Peek();

                foreach (var node in vertex.Neighbors.Keys)
                {
                    if (node.Color == null)
                    {
                        onForwardPass?.Invoke(vertex, node);
                        stack.Push(node);
                        node.Color = true;
                    }
                    else
                    {
                        onBackwardEdge?.Invoke(vertex, node);
                    }
                }

                vertex.Color = false;
                onBackwardPass?.Invoke(stack.Pop(), stack.Peek());
            }
        }
    }
}