using System;
using System.Collections.Generic;

namespace BridgeSearch.Models
{
    public class Vertex : IEquatable<Vertex>
    {
        public Vertex(int index)
        {
            Index = index;
            Neighbors = new Dictionary<Vertex, ulong>();
        }

        public bool? Color { get; set; }

        public int Index { get; }

        public int Tin { get; set; }
        public int Low { get; set; }

        public IDictionary<Vertex, ulong> Neighbors { get; }

        public bool Equals(Vertex other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Index == other.Index;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Vertex) obj);
        }

        public override int GetHashCode()
        {
            return Index;
        }

        public override string ToString()
        {
            return Index.ToString();
        }
    }
}