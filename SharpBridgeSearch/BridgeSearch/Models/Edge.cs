using System;

namespace BridgeSearch.Models
{
    public class Edge : Tuple<Vertex, Vertex>
    {
        public Edge(Vertex item1, Vertex item2, ulong coeff) : base(item1, item2)
        {
            Coeff = coeff;
        }

        public ulong Coeff { get; }
    }
}