using System;
using System.Collections.Generic;
using System.Linq;
using BridgeSearch.Models;

namespace BridgeSearch
{
    public static class Extensions
    {
        public static ulong NextULong(this Random random, ulong min = ulong.MinValue, ulong max = ulong.MaxValue)
        {
            var uRange = max - min;
            ulong ulongRand;
            do
            {
                byte[] buf = new byte[8];
                random.NextBytes(buf);
                ulongRand = BitConverter.ToUInt64(buf, 0);
            } while (ulongRand > ulong.MaxValue - (ulong.MaxValue % uRange + 1) % uRange);

            return ulongRand % uRange + min;
        }

        public static IEnumerable<Edge> GetEdges(this Graph graph)
        {
            return graph.Vertices.SelectMany(v => v.Neighbors
                .Where(n => n.Key.Index > v.Index)
                .Select(n => new Edge(v, n.Key, v.Neighbors[n.Key])));
        }
    }
}