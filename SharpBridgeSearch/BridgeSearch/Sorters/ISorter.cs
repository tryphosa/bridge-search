using System.Collections.Generic;
using BridgeSearch.Models;

namespace BridgeSearch.Sorters
{
    interface IEdgeSorter
    {
        IEnumerable<Edge> Sort(IEnumerable<Edge> source);
    }
}