﻿using System.Collections.Generic;
using System.Linq;
using BridgeSearch.Models;

namespace BridgeSearch.Sorters
{
    public class DefaultEdgeSorter : IEdgeSorter
    {
        public IEnumerable<Edge> Sort(IEnumerable<Edge> source)
        {
            var result = source.OrderBy(e => e.Coeff);
            return result;
        }
    }
}