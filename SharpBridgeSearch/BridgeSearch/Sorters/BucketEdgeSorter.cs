using System;
using System.Collections.Generic;
using System.Linq;
using BridgeSearch.Models;

namespace BridgeSearch.Sorters
{
    public class BucketEdgeSorter : IEdgeSorter
    {
        private int numOfBuckets;

        public BucketEdgeSorter(int numOfBuckets = 0)
        {
            this.numOfBuckets = numOfBuckets;
        }

        public IEnumerable<Edge> Sort(IEnumerable<Edge> source)
        {
            var sourceArray = source.ToArray();
            var sortedArray = new List<Edge>();
            // use the square root of the number of elements by default
            if (numOfBuckets <= 0)
            {
                numOfBuckets = Convert.ToInt32(Math.Sqrt(sourceArray.Length));
            }

            // Create buckets
            var buckets = new List<Edge>[numOfBuckets];
            for (var i = 0; i < numOfBuckets; i++)
            {
                buckets[i] = new List<Edge>();
            }

            // Iterate through the passed array and add each element to the appropriate bucket
            for (var i = 0; i < sourceArray.Length; i++)
            {
                // for numbers in [a, b) => floor(n*(A[i]-a)/(b-a))
                var div = (double) sourceArray[i].Coeff / ulong.MaxValue;
                var bucket = Convert.ToInt32(Math.Floor(numOfBuckets * div));
                buckets[bucket].Add(sourceArray[i]);
            }

            // Sort each bucket and add it to the result List
            for (var i = 0; i < numOfBuckets; i++)
            {
                var temp = InsertionSort(buckets[i]);
                //var temp = buckets[i].OrderBy(x => x.Coeff);
                sortedArray.AddRange(temp);
            }

            return sortedArray.ToArray();
        }

        private static List<Edge> InsertionSort(List<Edge> inputArray)
        {
            for (var i = 1; i < inputArray.Count; i++)
            {
                // Store the current element in a variable
                var currentValue = inputArray[i];
                var pointer = i - 1;

                // As long as we are pointing to a valid value in the array
                while (currentValue.Coeff < inputArray[pointer].Coeff)
                {
                    inputArray[pointer + 1] = inputArray[pointer];
                    inputArray[pointer] = currentValue;
                }
            }

            return inputArray;
        }
    }
}