using System;
using System.Collections.Generic;
using System.Linq;
using BridgeSearch.Models;

namespace BridgeSearch.Sorters
{
    public class RadixEdgeSorter : IEdgeSorter
    {
        public IEnumerable<Edge> Sort(IEnumerable<Edge> source)
        {
            var collection = source.ToArray();
            // helper array 
            var tmp = new Edge[collection.Length];
            // number of bits our group will be long 
            var groupBits = 16;
            // number of bits of a C# ulong 
            var bits = 64;

            // counting and prefix arrays (note dimensions 2^16 which is the number of all possible values of a 16-bit number) 
            var count = new ulong[1 << groupBits];
            var pref = new ulong[1 << groupBits];

            // number of groups 
            var groups = (int) Math.Ceiling(bits / (double)groupBits);

            // the mask to identify groups 
            var mask = (ulong) (1 << groupBits) - 1;

            for (int c = 0, shift = 0; c < groups; c++, shift += groupBits)
            {
                // reset count array 
                for (var j = 0; j < count.Length; j++)
                {
                    count[j] = 0;
                }

                // counting elements of the c-th group 
                for (var i = 0; i < collection.Length; i++)
                {
                    count[(collection[i].Coeff >> shift) & mask]++;
                }

                // calculating prefixes 
                pref[0] = 0;
                for (var i = 1; i < count.Length; i++)
                {
                    pref[i] = pref[i - 1] + count[i - 1];
                }

                // from collection[] to tmp[] elements ordered by c-th group 
                for (var i = 0; i < collection.Length; i++)
                {
                    tmp[pref[(collection[i].Coeff >> shift) & mask]++] = collection[i];
                }

                // collection[]=tmp[] and start again until the last group 
                tmp.CopyTo(collection, 0);
            }

            return collection;
        }
    }
}