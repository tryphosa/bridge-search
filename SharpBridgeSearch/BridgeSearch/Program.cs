﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms.DataVisualization.Charting;
using BridgeSearch.Models;
using BridgeSearch.Searchers;
using BridgeSearch.Sorters;
using NUnit.Framework;

namespace BridgeSearch
{
    internal class Program
    {
        public static readonly Random RNG = new Random(Guid.NewGuid().GetHashCode());
        private static readonly RandomBridgeSearcher randomBridgeSearcher = new RandomBridgeSearcher();
        private static readonly DeterminedBridgeSearcher determinedBridgeSearcher = new DeterminedBridgeSearcher();

        // private static int currentTime = 0;

        public static void Main(string[] args)
        {
            var size = int.Parse(args[0]);
            var p = double.Parse(args[1]);
            var filePath = args[2];

            var graph = GraphGenerator.Generate(size, p);

            var randomBridgeTime = TotalSeconds(() => randomBridgeSearcher.SearchBridges(graph));
            ResetColors(graph);
            var determinedBridgeTime = TotalSeconds(() => determinedBridgeSearcher.SearchBridges(graph));
            ResetColors(graph);
            var twoBridgeRadixTime =
                TotalSeconds(() => randomBridgeSearcher.SearchTwoBridges(graph, new RadixEdgeSorter()));
            ResetColors(graph);

            var twoBridgeBucketTime =
                TotalSeconds(() => randomBridgeSearcher.SearchTwoBridges(graph, new BucketEdgeSorter()));
            ResetColors(graph);
            var twoBridgeDefaultTime =
                TotalSeconds(() => randomBridgeSearcher.SearchTwoBridges(graph, new DefaultEdgeSorter()));

            using (var writer = new StreamWriter(filePath, append:true) {AutoFlush = true,})
            {
                writer.WriteLine(
                    $"{size};{randomBridgeTime};{determinedBridgeTime};{twoBridgeBucketTime};{twoBridgeRadixTime};{twoBridgeDefaultTime}");
            }
        }

        public static void TestSorters(string[] args)
        {
            var size = int.Parse(args[0]);
            var p = double.Parse(args[1]);
            var filePath = args[2];

            var graph = GraphGenerator.Generate(size, p);

            var radixEdgeSorter = new RadixEdgeSorter();
            var bucketEdgeSorter = new BucketEdgeSorter();
            var defaultEdgeSorter = new DefaultEdgeSorter();

            var bucketTimer = Stopwatch.StartNew();
            var edges = graph.GetEdges().ToArray();
            var bucketResult = bucketEdgeSorter.Sort(edges).ToArray();
            var bucketTime = bucketTimer.Elapsed.TotalSeconds;

            var radixTimer = Stopwatch.StartNew();
            var radixResult = radixEdgeSorter.Sort(edges).ToArray();
            var radixTime = radixTimer.Elapsed.TotalSeconds;

            var defaultTimer = Stopwatch.StartNew();
            var defaultResult = defaultEdgeSorter.Sort(edges).ToArray();
            var defaultTime = defaultTimer.Elapsed.TotalSeconds;

            using (var writer = new StreamWriter(filePath, append: true) { AutoFlush = true, })
            {
                writer.WriteLine(
                    $"{size};{0};{0};{bucketTime};{radixTime};{defaultTime}");
            }
        }



        private static double TotalSeconds(Action action)
        {
            var timer = Stopwatch.StartNew();
            action();
            return timer.Elapsed.TotalSeconds;
        }

        public static void DrawBridgeResultsFromFile(string filePath, string oneBridgePath, string twoBridgePath, string combinedPath)
        {
            Chart chart = new Chart();
            chart.Height = 500;
            chart.Width = 1200;
            chart.Legends.Add(new Legend("Legend1"));
            chart.Legends["Legend1"].Title = "1 Bridge Search Results";
            Chart chart2 = new Chart();
            chart2.Height = 500;
            chart2.Width = 1200;
            chart2.Legends.Add(new Legend("Legend1"));
            chart2.Legends["Legend1"].Title = "2 Bridge Search Results";

            ChartArea ca = new ChartArea();
            ca.Name = "ChartArea";
            ca.AxisX.Title = "Size";
            ca.AxisY.Title = "Time";
            ca.AxisX.IntervalType = DateTimeIntervalType.Number;
            ca.AxisY.IntervalType = DateTimeIntervalType.Number;
            ca.AxisX.Minimum = 10;
            ca.AxisY.Minimum = 0;
            chart.ChartAreas.Add(ca);
            ChartArea ca2 = new ChartArea();
            ca2.Name = "ChartArea";
            ca2.AxisX.Title = "Size";
            ca2.AxisY.Title = "Time";
            ca2.AxisX.IntervalType = DateTimeIntervalType.Number;
            ca2.AxisY.IntervalType = DateTimeIntervalType.Number;
            ca2.AxisX.Minimum = 10;
            ca2.AxisY.Minimum = 0;
            chart2.ChartAreas.Add(ca2);

            Series randomOneSeries = new Series();
            randomOneSeries.Name = "Randomized";
            randomOneSeries.Color = Color.Orange;
            randomOneSeries.ChartType = SeriesChartType.Line;
            Series determOneSeries = new Series();
            determOneSeries.Name = "Determined";
            determOneSeries.Color = Color.Brown;
            determOneSeries.ChartType = SeriesChartType.Line;

            Series bucketSeries = new Series();
            bucketSeries.Name = "BucketSort";
            bucketSeries.Color = Color.Green;
            bucketSeries.ChartType = SeriesChartType.Line;
            Series radixSeries = new Series();
            radixSeries.Name = "RadixSort";
            radixSeries.Color = Color.Blue;
            radixSeries.ChartType = SeriesChartType.Line;
            Series orderBySeries = new Series();
            orderBySeries.Name = "DefaultSort";
            orderBySeries.Color = Color.Red;
            orderBySeries.ChartType = SeriesChartType.Line;

            var reader = File.OpenText(filePath);
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                var results = line.Split(';');
                var size = int.Parse(results[0]);

                randomOneSeries.Points.AddXY(size, double.Parse(results[1]));
                determOneSeries.Points.AddXY(size, double.Parse(results[2]));

                bucketSeries.Points.AddXY(size, double.Parse(results[3]));
                radixSeries.Points.AddXY(size, double.Parse(results[4]));
                orderBySeries.Points.AddXY(size, double.Parse(results[5]));
            }

            chart.Series.Add(randomOneSeries);
            chart.Series.Add(determOneSeries);
            chart.SaveImage(oneBridgePath, ChartImageFormat.Png);

            chart2.Series.Add(bucketSeries);
            chart2.Series.Add(radixSeries);
            chart2.Series.Add(orderBySeries);
            chart2.SaveImage(twoBridgePath, ChartImageFormat.Png);

            chart.Legends["Legend1"].Title = "Bridge Search Results";
            chart.Series.Add(bucketSeries);
            chart.Series.Add(radixSeries);
            chart.Series.Add(orderBySeries);
            chart.SaveImage(combinedPath, ChartImageFormat.Png);
        }

        private static void ResetColors(Graph graph)
        {
            foreach (var edge in graph.GetEdges())
            {
                edge.Item1.Color = null;
                edge.Item2.Color = null;
            }
        }
    }
}