using System.Linq;
using BridgeSearch.Models;

namespace BridgeSearch
{
    static class GraphGenerator
    {
        public static Graph Generate(int size, double edgeProbability)
        {
            var graph = new Graph(Enumerable.Range(1, size).Select(n => new Vertex(n)));

            foreach(var u in graph.Vertices)
            foreach (var v in graph.Vertices)
            {
                if (u.Equals(v) || Program.RNG.NextDouble() >= edgeProbability)
                {
                    continue;
                }

                var weight = Program.RNG.NextULong();
                u.Neighbors[v] = weight;
                v.Neighbors[u] = weight;
            }

            return graph;
        }
    }
}