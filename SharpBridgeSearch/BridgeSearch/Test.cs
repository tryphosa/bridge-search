using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using BridgeSearch.Models;
using BridgeSearch.Searchers;
using NUnit.Framework;

namespace BridgeSearch
{
    [TestFixture]
    public class Test
    {
        [Test]
        public void MeasureTime()
        {
            for (var i = 10; i <= 1000; i = i + 10)
            {
                Program.Main(new[] {$"{i}", $"{1.0 / i}", "BridgeSearchResults/results1000.csv" });
            }
        }

        [Test]
        public void MeasureSortersTime()
        {
            for (var i = 100; i <= 10000; i = i + 100)
            {
                Program.TestSorters(new[] { $"{i}", $"{1.0 / i}", "BridgeSearchResults/results_sorters10000.csv" });
            }
        }

        [Test]
        public void DrawSortersResultsFromFile()
        {
            Program.DrawBridgeResultsFromFile("BridgeSearchResults/results_sorters10000.csv",
                "BridgeSearchResults/results_sorters10000.png",
                "BridgeSearchResults/results_sorters10000.png",
                "BridgeSearchResults/results_sorters10000.png");
        }

        [Test]
        public void DrawBridgeResultsFromFile()
        {
            Program.DrawBridgeResultsFromFile("BridgeSearchResults/results1000.csv", 
                "BridgeSearchResults/oneBridgeResults1000.png", 
                "BridgeSearchResults/twoBridgeResults1000.png",
                "BridgeSearchResults/combined1000.png");
        }

        [Test]
        public void TestDeterminedBridgeSearcherTriangle()
        {
            for (var i = 0; i < 100; i++)
            {
                var graph = new Graph(new Vertex[0]);
                var edges = new Edge[0];
                while (!edges.Any() || edges.Length == 3)
                {
                    graph = GraphGenerator.Generate(3, 1.0 / 3);
                    edges = graph.GetEdges().ToArray();
                }
                var determinedBridgeSearcher = new DeterminedBridgeSearcher();
                var result = determinedBridgeSearcher.SearchBridges(graph);
                Assert.True(result.Length == edges.Length,$"{i}");
            }
        }

        [Test]
        public void TestDeterminedBridgeSearcherSingleEdge()
        {
            for (var i = 0; i < 100; i++)
            {
                var graph = new Graph(new Vertex[0]);
                var edges = new Edge[0];
                while (!edges.Any())
                {
                    graph = GraphGenerator.Generate(2, 1.0);
                    edges = graph.GetEdges().ToArray();
                }
                //var timer = Stopwatch.StartNew();
                var determinedBridgeSearcher = new DeterminedBridgeSearcher();
                var result = determinedBridgeSearcher.SearchBridges(graph);
                Assert.True(result.Length == edges.Length);
            }
        }
    }
}