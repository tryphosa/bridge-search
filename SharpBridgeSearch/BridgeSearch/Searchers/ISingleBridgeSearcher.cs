﻿using System;
using System.Collections.Generic;
using BridgeSearch.Models;

namespace BridgeSearch.Searchers
{
    interface ISingleBridgeSearcher
    {
        Edge[] SearchBridges(Graph graph);
    }
}