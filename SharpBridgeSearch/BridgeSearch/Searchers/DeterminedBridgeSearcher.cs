﻿using System;
using System.Collections.Generic;
using System.Linq;
using BridgeSearch.Models;

namespace BridgeSearch.Searchers
{
    public class DeterminedBridgeSearcher : ISingleBridgeSearcher
    {
        public Edge[] SearchBridges(Graph graph)
        {
            var time = 0;
            var result = new List<Edge>();
            graph.DepthFirstSearch(
                onForwardPass: (u, v) =>
                {
                    time++;
                    v.Tin = time;
                    v.Low = time;
                },
                onBackwardEdge: (u, v) => u.Low = Math.Min(u.Low, v.Tin),

                onBackwardPass: (u, v) =>
                {
                    if (u != null && v.Low > u.Tin)
                    {
                        result.Add(new Edge(u, v, 0));
                    }
                }
            );

            return result.ToArray();
        }
    }
}