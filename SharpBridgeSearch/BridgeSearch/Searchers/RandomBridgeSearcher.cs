using System;
using System.Collections.Generic;
using System.Linq;
using BridgeSearch.Models;
using BridgeSearch.Sorters;
using System.Runtime.InteropServices;

namespace BridgeSearch.Searchers
{
    class RandomBridgeSearcher : ISingleBridgeSearcher
    {
        public Edge[] SearchBridges(Graph graph)
        {
            SetEdgeCoefficients(graph);

            var edges = graph.GetEdges();
            return edges.Where(e => e.Coeff == 0).ToArray();
        }

        public Tuple<Edge, Edge>[] SearchTwoBridges(Graph graph, IEdgeSorter edgeSorter)
        {
            SetEdgeCoefficients(graph);

            var edges = graph.GetEdges();

            var sortedEdges = edgeSorter.Sort(edges);

            var group = new List<Edge>();

            var result = new List<Tuple<Edge, Edge>>();

            foreach (var edge in sortedEdges)
            {
                var coeff = edge.Coeff;
                if (!group.Any() || coeff == group.First().Coeff)
                {
                    group.Add(edge);
                }
                else if (group.Any())
                {
                    for (var i = 0; i < group.Count; i++)
                    {

                        for (var j = i + 1; j < group.Count; j++)
                        {
                            result.Add(Tuple.Create(group[i], group[j]));
                        }
                    }

                    group.Clear();
                }
            }

            if (group.Any())
            {
                for (var i = 0; i < group.Count; i++)
                {
                    for (var j = i + 1; j < group.Count; j++)
                    {
                        result.Add(Tuple.Create(group[i], group[j]));
                    }
                }
            }

            return result.ToArray();
        }

        private void SetEdgeCoefficients(Graph graph)
        {
            graph.DepthFirstSearch(
                onBackwardPass: (v, parent) =>
                {
                    if (parent != null)
                    {
                        var weight = parent.Neighbors
                            .Where(vertex => !vertex.Key.Equals(v))
                            .Aggregate(0UL, (value, kvp) => value ^ kvp.Value);
                        parent.Neighbors[v] = weight;
                        v.Neighbors[parent] = weight;
                    }
                });
        }
    }
}