# Graph Bridge Search Algorithms

## Task
Compare determined and randomized bridge search algorithms for graphs of different density.

## Object-oriented approach
Graph is a class which contains collection (C# hash set) of edges.
Edge is a class which contains hash set of nodes.
Node indices are taken as their hashes.
Hash set compares edges by hashes of their nodes, so in graph there is no more than 1 edge for each pair of nodes.

## Graph generation
For graph generation we took Erdős–Rényi (n,p)-model. Dense graphs were built with probability of |V|/2/(|V|-1).
Sparse graphs were built with probability of 1/|V|.
This probability is a probability of edge occurrence for all possible edges in a graph of size |V|.
## Random search


### DFS
We used classic DFS search with parameters:
 - current node
 - parent node
 - onForwardPass edge event handler
 - onBackwardPass edge event handler

If there are unvisited nodes after DFS, we start new DFS pass from random unvisited node. After this procedure all nodes will be visited.

### Algorithm
The idea of randomized search is that bridges have equal coefficients in constraints system for optimization problem described in [paper](https://arxiv.org/pdf/cs/0702113.pdf).
 
## Determined search
### DFS
We used classic DFS search with parameters:
 - current node
 - parent node
 - onBackwardEdge node event handler

If there are unvisited nodes after DFS, we start new DFS pass from random unvisited node. After this procedure all nodes will be visited.
### Algorithm
Determined search algorithm is known for years: if we meet backward edge (edge that connects current vertex with a vertex we already met), then this edge is not a bridge. Thus, 1 full DFS pass is enough to determine bridges in a graph.
## Sparse graphs results
![](SharpBridgeSearch/BridgeSearchResults/results_sorters100000.png)
![](SharpBridgeSearch/BridgeSearchResults/combined15000.png)
## Dense graphs results
![](SharpBridgeSearch/BridgeSearchResults/results_sorters_dense_1600.png)
![](SharpBridgeSearch/BridgeSearchResults/combined1000.png)

## Executing locally
You should have any .NET Framework v4.5 compatible compiler to create Program.exe file of this application. After build, run `Program.exe <n> <p> <filePath>`, where `n` is the size of the generated graph, `p` is the probability of edge occurance, and `filePath` is path to a csv-file, where results should be written in process of execution.

*Done by Ilya Trushkin and Alexander Sergeev*
