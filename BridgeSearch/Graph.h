#include "Edge.h"
#include "vector"
#pragma once
using namespace std;

class Graph
{
public:
	Graph(vector<Edge> &edges);
	~Graph();
	vector<Edge> edges;
	void DepthFirstSearch();
private:
	Vertex startVertex;
	void DepthFirstSearch(Vertex *startVertex);
};



