#include "pch.h"
#include "Graph.h"
#include "iostream"
using namespace std;


Graph::Graph(vector<Edge> &edges) : edges(edges), 
startVertex(edges[rand() / RAND_MAX * edges.size()].from)
{
}

Graph::~Graph()
{
}

void Graph::DepthFirstSearch()
{
	this->DepthFirstSearch(&startVertex);
}

void Graph::DepthFirstSearch(Vertex *startVertex) {
	cout << "DFS reached v" << startVertex->index << "." << endl;
	startVertex->color = 1;
	for (int i = 0; i < edges.size(); i++) {
		Edge edge = edges[i];
		if (edge.from.index == startVertex->index && edge.to.color == 0) {
			edge.isSpanning = true;
			this->DepthFirstSearch(&edge.to);
		}
	}
	startVertex->color = 2;
}