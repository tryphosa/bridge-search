#pragma once

#include "Graph.h"

class GraphGenerator
{
public:
	GraphGenerator();
	~GraphGenerator();
	Graph Generate(int size, double edgeProbability);
	vector<Vertex> vertices;
	vector<Edge> edges;
};

