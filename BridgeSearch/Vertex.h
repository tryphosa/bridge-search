#pragma once
struct Vertex
{
public:
	Vertex(int index);
	~Vertex();
	int index;
	unsigned char color;
	void ChangeColor(unsigned char color);
};

