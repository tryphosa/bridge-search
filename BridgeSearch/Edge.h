#pragma once
#include "Vertex.h"

struct Edge
{
public:
	Edge(Vertex *from, Vertex *to);
	~Edge();
	Vertex from;
	Vertex to;
	bool isSpanning = false;
	unsigned long long value;
};

