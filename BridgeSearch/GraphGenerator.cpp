#include "pch.h"
#include "GraphGenerator.h"
#include <iostream>
#include <fstream>
#include <time.h> 
using namespace std;

GraphGenerator::GraphGenerator()
{
	srand(time(NULL));
	vertices = vector<Vertex>();
	edges = vector<Edge>();
}


GraphGenerator::~GraphGenerator()
{
}

Graph GraphGenerator::Generate(int size, double edgeProbability) {
	for (int i = 1; i <= size; i++) {
		vertices.push_back(Vertex(i));
	}

	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			if (j == i) continue;
			double p = (double)rand() / RAND_MAX;
			if (p < edgeProbability) {
				edges.push_back(Edge(&vertices[i], &vertices[j]));
			}
		}
	}

	return Graph(edges);
}
